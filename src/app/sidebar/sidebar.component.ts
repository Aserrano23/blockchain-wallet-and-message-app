import { Component, OnInit } from '@angular/core';
import { ContractService } from '../contract.service';
import { MessagesService } from '../messages.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styles: [`
      h4{
        margin-left: 5px;
        font-weight: bold;
      }
  `]
})
export class SidebarComponent implements OnInit {

  connected: boolean = false;
  wallet: string = '';
  allowed: boolean = true;
  constructor(private contractService: ContractService, private messageService: MessagesService) {
    
  }

  ngOnInit(): void {
    this.checkIsAllowed();
  }

  async checkIsAllowed(){
    await this.contractService.web3.eth.getAccounts().then(res => {
      this.wallet = res[0]
    });

    let contractToken = this.messageService.contractToken;
    let tokens = (await this.contractService.contract.methods.allowance(this.wallet, contractToken).call());

    console.log(`Address contract: ${contractToken}`);
    console.log(`Tokens allowed: ${tokens}`);
    console.log(`My wallet: ${this.wallet}`);

    if (tokens > 0) {
      this.allowed = true;
      console.log(`Allowed: ${this.allowed}`);
    } else {
      this.allowed = false;
      console.log(`Allowed: ${this.allowed}`);
    }
  }
  async allowUseTokens(){
    this.allowed = true;
    let contractToken = this.messageService.contractToken;
    this.contractService.contract.methods.approve(contractToken, "115792089237316195423570985008687907853269984665640564039457584007913129639935").send({from: this.wallet}).then(console.log);
  }
  /* smtm(){
    this.contractService.showMoney(this.wallet);
  } */
}
