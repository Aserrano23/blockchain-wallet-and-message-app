import { Component, Input, OnInit } from '@angular/core';
import { ContractService } from '../../contract.service';

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html'
})
export class TransferComponent implements OnInit {

  to:string = '';
  amount:string = '';
  status: string = '';
  wallet: string = '';
  
  constructor(private contractService: ContractService) {}

  ngOnInit(): void {

  }

  async doATransfer(to:string, amount: string){

    await this.contractService.web3.eth.getAccounts().then(res => {
      this.wallet = res[0];
    })

    this.status = ''

    if (to != '' && amount != '') {

      this.contractService.contract.methods.transfer(to,amount).send({from: this.wallet})
    } else {

      this.status = 'Los 2 campos deben estar rellenados.'

    }

    this.to = '';
    this.amount = '';
  }

}
