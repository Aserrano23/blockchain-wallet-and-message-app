import { Component, OnInit } from '@angular/core';
import Web3 from 'web3';
import { ContractService } from '../../contract.service';

@Component({
  selector: 'app-balance',
  templateUrl: './balance.component.html',
  styles:[`a{text-decoration: none}`]
})
export class BalanceComponent implements OnInit {

  web3 = new Web3(window['ethereum']);

  wallet: string = '';
  tokens: string = '0';
  network: string = '';
  eth: string = '';

  constructor(private contractService: ContractService) {}

  ngOnInit(){
    this.getWalletData();
  }

  async getWalletData(){
    

    await this.contractService.web3.eth.getAccounts().then(res => {
      this.wallet = res[0];
    })

    this.contractService.contract.methods.balanceOf(this.wallet).call((err:any, res:any) =>{
      if (!err) {
        this.tokens = res;
      }else {
        console.log(err);
      }
    })

    this.web3.eth.net.getNetworkType((err:any, res:any) => {
      this.network = res;
    })
  }

  transformTokens(stringTokens: string): string{
    let tokens = this.web3.utils.fromWei(stringTokens, 'ether')

    return tokens.toString();
  }
}
