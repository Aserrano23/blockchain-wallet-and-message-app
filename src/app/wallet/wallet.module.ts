import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransferComponent } from './transfer/transfer.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    TransferComponent, 
    TransactionsComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ]
})
export class WalletModule { }
