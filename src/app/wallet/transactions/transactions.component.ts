import { Component, OnInit } from '@angular/core';
import { ContractService } from '../../contract.service';
import { Tx } from '../../interfaces/tx.interface';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styles: [`
  span,h2{
    font-weight: bold;
  }
  `]
})
export class TransactionsComponent implements OnInit {

  wallet: string = '';
  txSent: Tx[] = [];
  txReceived: Tx[] = [];
  valueEth: string = '';

  constructor(private contractService: ContractService) {}

  ngOnInit(): void {
    this.seeTransactions();
  }

  async seeTransactions(){
    await this.contractService.web3.eth.getAccounts().then(res => {
      this.wallet = res[0];
    })

    this.contractService.contract.getPastEvents('Transfer', {
      filter: { from: this.wallet },
      fromBlock: 0,
      toBlock: 'latest'
    }, (error: any, event: any) =>{ 
      this.txSent = event;
      console.log(this.txSent);
    })

    this.contractService.contract.getPastEvents('Transfer', {
      filter: { to: this.wallet },
      fromBlock: 0,
      toBlock: 'latest'
    }, (error: any, event: any) =>{ 
      this.txReceived = event;
      console.log(this.txReceived);
    })
  }
}
