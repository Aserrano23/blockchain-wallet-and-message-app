import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { NewMessageComponent } from './new-message/new-message.component';
import { ListMessagesComponent } from './list-messages/list-messages.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FormsModule } from '@angular/forms';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { environment } from '../environments/environment';
import { WalletModule } from './wallet/wallet.module';
import { RouterModule } from '@angular/router';
import { BuyASCComponent } from './buy-asc/buy-asc.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    NewMessageComponent,
    ListMessagesComponent,
    SidebarComponent,
    BuyASCComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    WalletModule,
    CommonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
