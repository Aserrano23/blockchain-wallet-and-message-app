import { Component, OnInit } from '@angular/core';
import { Message } from '../interfaces/message.interface';
import { MessagesService } from '../messages.service';
import { ContractService } from '../contract.service';
import { threadId } from 'node:worker_threads';

@Component({
  selector: 'app-list-messages',
  templateUrl: './list-messages.component.html',
  styles: [
    `
      i{
        cursor: pointer;
      }
    `
  ]
})
export class ListMessagesComponent implements OnInit {
  
  // messages: Message[] = this.messagesService.getMessages;
  array: any = [];
  messages: Message[] = [];
  depuredMessages: Message[] = [];
  message!: Message;
  wallet: string = '';
  editTitle: string = '';
  editText: string = '';
  editId: number = 0;
  status: string = '';

  constructor(
    private messagesService: MessagesService) { 
      this.messagesService.web3.eth.getAccounts().then(res=>{
        this.wallet = res[0];
      })
    }

  ngOnInit(): void {
    this.listMessages();
  }

  depureMessages(){
    this.messages.forEach(msg => {
      if (msg.title != '' && msg.text != '' && msg.id != '') {
        this.depuredMessages.push(msg);
      }
    });
  }

  async listMessages(){
    this.messages = await this.messagesService.contract.methods.listMessages().call();
    this.depureMessages();
  }

  async deleteMessage(i: number){
    let ownerWallet = (await this.messagesService.contract.methods.owner().call());
    
    if (this.wallet !== ownerWallet) {
      this.status = "You are not the owner of the contract, so you can't delete a message, you only can update and list them.";
      return;
    } else {
      this.messagesService.deleteMessage(parseInt(this.depuredMessages[i].id), this.wallet);
    }
    
  }

  getData(title: string, text: string, id: string){
    this.editTitle = title;
    this.editText = text;
    this.editId = parseInt(id);
  }

  editMessage(){
    this.messagesService.updateMessage(this.editTitle, this.editText, this.editId , this.wallet);
  }
}
