import { Injectable, OnInit } from '@angular/core';
import Web3 from 'web3';

declare global {
  interface Window {
    ethereum: any;
  }
}

@Injectable({
  providedIn: 'root'
})

export class ContractService implements OnInit {

  web3 = new Web3(window['ethereum']);

  abiToken = require('src/abi-ASC.json');
  abiSendASC = require('src/abiSend.json');

  //Contract to buy ASC
  contractSendASCToken: string = '0xdB56641E77A03Ba5213554b17B246Fc2e8c06516';

  //ASC TOKEN
  contractToken: string = '0xbB137243613ea0Fd4b65A98a2E689d8776236c93';

  contract = new this.web3.eth.Contract([]);
  contractSendASC = new this.web3.eth.Contract([]);

  constructor() {
    this.contract = new this.web3.eth.Contract(this.abiToken,this.contractToken);
    this.contractSendASC = new this.web3.eth.Contract(this.abiSendASC,this.contractSendASCToken);
    this.connectToMetamask();
  }

  ngOnInit(): void {}

  async connectToMetamask(){
    await window['ethereum'].enable();
  }
}
