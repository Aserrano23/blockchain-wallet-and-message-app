import { Component, Input, OnInit } from '@angular/core';
import { Message } from '../interfaces/message.interface';
import { MessagesService } from '../messages.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-message',
  templateUrl: './new-message.component.html',
  styles: [
  ]
})
export class NewMessageComponent implements OnInit {


  wallet: string = '';
  status: string = '';

  @Input()
  message: Message = {
    id: '',
    title: '',
    text: ''
  }
  constructor(
    private messagesService:MessagesService,
    private router: Router) { 
      this.messagesService.web3.eth.getAccounts().then(res => {
        this.wallet = res[0];
      })
    }

  ngOnInit(): void {
  }

  async createMessage(){
    if (this.message.title.trim().length === 0) { 
      this.status = "The title field can't be empty."
      return;
    }

    if (this.message.text.trim().length === 0) { 
      this.status = "The text field can't be empty."
      return;
     }

    let ownerWallet = (await this.messagesService.contract.methods.owner().call());
    
    if (this.wallet !== ownerWallet) {
      this.status = "You are not the owner of the contract, so you can't create a new message, you only can update and list them.";
      return;
    } else {
      this.messagesService.createMessage(this.message, this.wallet);

      this.message = {
        id: '',
        title: '',
        text: ''
      }
    }
  }
}
