import { NgModule } from '@angular/core';
import { RouterModule, Routes} from "@angular/router";
import { NewMessageComponent } from '../new-message/new-message.component';
import { ListMessagesComponent } from '../list-messages/list-messages.component';
import { BalanceComponent } from '../wallet/balance/balance.component';
import { TransactionsComponent } from '../wallet/transactions/transactions.component';
import { TransferComponent } from '../wallet/transfer/transfer.component';
import { BuyASCComponent } from '../buy-asc/buy-asc.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'newMessage',
    pathMatch: 'full'
  },
  {
    path: 'newMessage',
    component: NewMessageComponent
  },
  {
    path: 'listMessages',
    component: ListMessagesComponent
  },
  {
    path: 'wallet',
    component: BalanceComponent
  },
  {
    path: 'transfer',
    component: TransferComponent
  },
  {
    path: 'transactions',
    component: TransactionsComponent
  },
  {
    path: 'buyASC',
    component: BuyASCComponent
  },
  {
    path: '**',
    redirectTo: ''
  }
]

@NgModule({
  imports:[
    RouterModule.forRoot(routes)
  ],
  exports:[
      RouterModule
  ]
})
export class AppRoutingModule { }