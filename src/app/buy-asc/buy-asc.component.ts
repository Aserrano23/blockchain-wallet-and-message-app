import { Component, OnInit } from '@angular/core';
import { ContractService } from '../contract.service';

@Component({
  selector: 'app-buy-asc',
  templateUrl: './buy-asc.component.html'
})
export class BuyASCComponent implements OnInit {

  value!: number;
  wallet: string = '';
  status: string = '';

  constructor(private contractservice: ContractService) { 
    this.contractservice.web3.eth.getAccounts().then((res: any) => {
      this.wallet = res[0];
    })
  }

  ngOnInit(): void {
  }

  async buyASC(){
    let owner = (await this.contractservice.contractSendASC.methods.owner().call());
    if (this.value) {
      let valueWei = this.value * 1000000000000000000;
      
      this.contractservice.contractSendASC.methods.sendTokens(owner).send({from: this.wallet, value: valueWei})
    } else {
      this.status = "Quantity cannot be empty";
    }
  }

}
