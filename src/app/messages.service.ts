import { Injectable } from '@angular/core';
import { Message } from './interfaces/message.interface';
import Web3 from 'web3';
import { Router } from '@angular/router';
import { ContractService } from './contract.service';

declare global {
  interface Window {
    ethereum: any;
  }
}

@Injectable({
  providedIn: 'root'
})

export class MessagesService {

  web3 = new Web3(window['ethereum']);

  abi = require('src/abiMsg.json');

  contractToken: string = '0x5145133f5d96bc2132703ba9Bd23509bb8c92B58';

  contract = new this.web3.eth.Contract([])

  messages: Message[] = []

  constructor(private router: Router, private contractService: ContractService) {
    this.contract = new this.web3.eth.Contract(this.abi,this.contractToken);
    this.connectToMetamask();
  }

  ngOnInit(): void {
  }

  async connectToMetamask(){
    await window['ethereum'].enable();
    
    window.ethereum.on('accountsChanged', function () {
      window.location.reload();
    });

    window.ethereum.on('chainChanged', function () {
      window.location.reload();
    });
  }

  public createMessage(message: Message, wallet:string) {
    let id = (this.messages.length).toString();
    let title = message.title;
    let text = message.text;
    this.contract.methods.createMessage(id,title,text).send({from: wallet});
  }

  async listMessages(){
    this.messages = await this.contract.methods.listMessages().call();
  }

  async deleteMessage(i: number, wallet: string){
    await this.contract.methods.deleteMessage(i).send({from: wallet})    
  }

  async updateMessage(title: string, text: string, id: number, wallet: string) {
    let i = id.toString();

    await this.contract.methods.updateMessage(i, title, text, id).send({from: wallet});
  }
}
